import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Gestion {

    static Scanner sc;
    static Problemas problemas;

    public static void main(String[] args) throws IOException {

        System.out.println(">>>>>>>>>>> Para que el programa reconozca los problemas, cada Java debe llevar en la primera linea un comentario (con un espacio despues de las dos barras) con el siguiente formato: id-titulo-veredicto-url.");
        System.out.println(">>>>>>>>>>> EJEMPLO DE PRIMERA LINEA DE UN JAVA: // 313-Fin De Mes-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=313");
        System.out.println(">>>>>>>>>>> Si el formato no es correcto, el programa avisará del archivo exacto que no está bien formateado.");

        problemas = new Problemas();
        sc = new Scanner(System.in);

        int opcion = -1;
        while(opcion != 4){
            System.out.println(">>> Cargando información...");
            gatherEverything();
            Collections.sort(problemas.getProblemas());
            System.out.println(">>> Información completada.");

            System.out.println("-------------------------------------");
            System.out.println("Total problemas registrados: " + problemas.getProblemas().size());
            System.out.println("-------------------------------------");
            System.out.println("(1) Mostrar resueltos.");
            System.out.println("(2) Mostrar por resolver.");
            System.out.println("(3) Mostrar todos.");
            System.out.println("(4) Salir.");
            System.out.println("-------------------------------------");

            opcion = sc.nextInt();

            switch (opcion){
                case 1: {
                    showCompleted();
                    break;
                }
                case 2: {
                    showIncompleted();
                    break;
                }
                case 3: {
                    showAll();
                    break;
                }
                case 4: {
                    break;
                }
                default: {
                    System.out.println("Opción no reconocida.");
                    break;
                }
            }
        }


    }

    private static void gatherEverything() throws IOException {
        ArrayList<Problema> ps = new ArrayList<>();

        File mainFolder = new File("src/Volumenes");
        File[] folders = mainFolder.listFiles();

        boolean errors = false;
        for (File f : folders){
            File[] javas = f.listFiles();
            int length = javas.length;
            for (int i = 0; i < length; i++) {
                File problema = javas[i];

                BufferedReader br = new BufferedReader(new FileReader(problema));
                String linea = br.readLine();

                String[] split = linea.split("-");
                if (split.length != 4){
                    errors = true;
                    System.out.println("(WARNING) Comprueba el formato de la primera linea de: " + problema.getPath());
                } else {
                    String tmpId = split[0];
                    int id = Integer.parseInt(tmpId.substring(3));
                    String titulo = split[1];
                    String veredicto = split[2];
                    String url = split[3];

                    Problema p = new Problema(id, titulo, veredicto, url);
                    ps.add(p);
                }
            }
        }

        if (!errors){
            System.out.println("Sin errores.");
        }

        problemas.setProblemas(ps);
    }

    private static void columnNames(){
        System.out.println("ID    Titulo                                             Veredicto                      URL");
        System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------------");
    }

    private static void showAll() {
        columnNames();
        ArrayList<Problema> p = problemas.getProblemas();

        for (int i = 0; i < p.size(); i++) {
            System.out.println(p.get(i));
        }
    }

    private static void showIncompleted() {
        columnNames();
        ArrayList<Problema> p = problemas.getProblemas();

        for (int i = 0; i < p.size(); i++) {
            Problema pr = p.get(i);
            if (!pr.getVeredicto().equalsIgnoreCase("accepted")){
                System.out.println(pr);
            }
        }
    }

    private static void showCompleted() {
        columnNames();
        ArrayList<Problema> p = problemas.getProblemas();

        for (int i = 0; i < p.size(); i++) {
            Problema pr = p.get(i);
            if (pr.getVeredicto().equalsIgnoreCase("accepted")){
                System.out.println(pr);
            }
        }
    }

}

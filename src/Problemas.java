import java.io.Serializable;
import java.util.ArrayList;

public class Problemas implements Serializable {

    ArrayList<Problema> problemas = new ArrayList<>();

    public ArrayList<Problema> getProblemas() {
        return problemas;
    }

    public void setProblemas(ArrayList<Problema> problemas) {
        this.problemas = problemas;
    }
}

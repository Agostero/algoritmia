import java.io.Serializable;

public class Problema implements Serializable, Comparable<Problema> {
    int id;
    String titulo;
    String veredicto;
    String url;

    public Problema(int id, String titulo, String veredicto, String url) {
        this.id = id;
        this.titulo = titulo;
        this.veredicto = veredicto;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getVeredicto() {
        return veredicto;
    }

    public void setVeredicto(String veredicto) {
        this.veredicto = veredicto;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        String out = String.format("%-5s %-50s %-30s %-150s", "[" + id + "]", titulo, veredicto, url);

        return out;
    }

    @Override
    public int compareTo(Problema o) {
        if (id > o.id){
            return 1;
        } else {
            return -1;
        }
    }
}

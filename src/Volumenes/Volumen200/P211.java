// 211-Aprendiendo El Codigo Morse-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=211
package Volumenes.Volumen200;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P211 {

    static class Morse{
        char letra;
        char minuscula;
        String morse;

        public Morse(char letra, char minuscula, String morse) {
            this.letra = letra;
            this.minuscula = minuscula;
            this.morse = morse;
        }
    }

    public static BufferedReader br;
    static Morse[] abcMorse = {new Morse('A','a', ".-"), new Morse('B','b', "-..."), new Morse('C','c', "-.-."),
            new Morse('D','d', "-.."),new Morse('E','e', "."),new Morse('F','f', "..-.") ,new Morse('G','g', "--.") ,
            new Morse('H','h', "...."), new Morse('I','i', ".."), new Morse('J','j', ".---"),  new Morse('K','k', "-.-"),
            new Morse('L','l', ".-.."), new Morse('M','m', "--"), new Morse('N','n', "-."), new Morse('O','o', "---"),
            new Morse('P','p', ".--."), new Morse('Q','q', "--.-"), new Morse('R','r', ".-."), new Morse('S','s', "..."),
            new Morse('T','t', "-"), new Morse('U','u', "..-"), new Morse('V','v', "...-"), new Morse('W','w', ".--")
            , new Morse('X','x', "-..-"), new Morse('Y','y', "-.--"),  new Morse('Z','z', "--..")};

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (linea != null){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String palabra){
        String morse = "";
        char[] arr = palabra.toCharArray();
        char letra = arr[0];
        int length = arr.length;
        for (int i = 0; i < length; i++) {
            char c = arr[i];
            if (c == 'o' || c == 'O'){
                morse += '-';
            } else if (c == 'a' || c == 'e' || c == 'i' || c == 'u' || c == 'A' || c == 'E' || c == 'I' || c == 'U'){
                morse += '.';
            }
        }

        for (int i = 0; i < abcMorse.length; i++) {
            Morse m = abcMorse[i];

            if ((m.letra == letra || m.minuscula == letra) && m.morse.equals(morse)){
                System.out.println(palabra + " OK");
                return;
            }
        }

        System.out.println(palabra + " X");
    }
}

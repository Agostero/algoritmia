// 428-Tendencia Al Lado Oscuro-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=428&cat=108
package Volumenes.Volumen400;

import java.util.Scanner;

public class P428 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int casos = sc.nextInt();

        for (int i = 0; i < casos; i++) {
            String midiclorianos = baseTenToFive(sc.nextInt());

            String answer = getFours(midiclorianos);
            System.out.println(answer);
        }
    }

    public static String getFours(String midiclorianos){
        String[] sArray = midiclorianos.split("");

        int sum = 0;

        for (int i = 0; i < sArray.length; i++) {
            if (sArray[i].equals("4")){
                sum++;
            }
        }

        if (sum >= 2){
            return "SI";
        } else {
            return "NO";
        }
    }

    public static String baseTenToFive(int num){
        return Integer.toString(num, 5);
    }
}

// 475-Cameos Stan Lee-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=475&cat=117
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P475 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int casos = Integer.parseInt(br.readLine());

        String[] letras = {"s", "t", "a", "n", "l", "e", "e"};
        int indice = 0;
        int cantidad = 0;
        for (int i = 0; i < casos; i++) {
            String linea = br.readLine();
            String[] lineaArray = linea.split("");
            int length = lineaArray.length;
            for (int j = 0; j < length; j++) {
                String charAt = lineaArray[j];
                if (charAt.equalsIgnoreCase(letras[indice])){
                    if (indice == 6){
                        indice = 0;
                        cantidad++;
                    } else {
                        indice++;
                    }
                }
            }
            System.out.println(cantidad);
            cantidad = 0;
            indice = 0;
        }
    }
}

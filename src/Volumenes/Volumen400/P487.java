// 487-Giratiempo-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=487
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class P487 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        int n = Integer.parseInt(linea);
        for (int i = 0; i < n; i++) {
            caso();
        }
    }

    static void caso() throws IOException {
        BigInteger n = new BigInteger(br.readLine());

        n = n.multiply(new BigInteger("3600"));
        BigInteger result = n.divide(new BigInteger("3599"));
        BigInteger rest = n.remainder(new BigInteger("3599"));

        if (rest.compareTo(new BigInteger("0")) == 0){
            System.out.println(result.subtract(new BigInteger("1")));
        } else {
            System.out.println(result);
        }

    }
}

// 481-Ajedrez Asistido Por Computador-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=481
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P481 {
    public static BufferedReader br;
    static String[] filas = {"h", "g", "f", "e", "d", "c", "b", "a"};

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (!linea.equals("0 0")){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String linea){
        String[] split = linea.split(" ");
        int n1 = Integer.parseInt(split[0]);
        int n2 = Integer.parseInt(split[1]);

        String letra = filas[n1 - 1];

        System.out.println(letra + n2);

    }
}

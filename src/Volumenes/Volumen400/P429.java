// 429-Organizando Los Hangares-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=429&cat=108
package Volumenes.Volumen400;

import java.io.IOException;
import java.util.Scanner;

public class P429 {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);

        while(true){
            int h = sc.nextInt();

            if (h == 0){
                break;
            }

            int[] hangares = new int[h];
            for (int i = 0; i < h; i++) {
                hangares[i] = sc.nextInt();
            }

            int n = sc.nextInt();

            int[] naves = new int[n];
            for (int i = 0; i < n; i++) {
                naves[i] = sc.nextInt();

                int indexOfHangar = findIndexOfHangarWithMoreSpace(hangares);
                hangares[indexOfHangar] -= naves[i];
            }

            boolean fits = true;
            for (int i : hangares){
                if (i < 0){
                    fits = false;
                }
            }

            if (fits){
                System.out.println("SI");
            } else {
                System.out.println("NO");
            }
        }
    }

    public static int findIndexOfHangarWithMoreSpace(int[] hangares){
        int max = hangares[0];
        int index = 0;

        int length = hangares.length;
        for (int i = 0; i < length; i++) {
            if (hangares[i] > max){
                max = hangares[i];
                index = i;
            }
        }

        return index;
    }
}

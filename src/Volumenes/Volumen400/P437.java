// 437-El Anuncio Mas Caro Del Año-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=437&cat=111
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P437 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        int casos = Integer.parseInt(br.readLine());
        for (int i = 0; i < casos; i++) {
            caso();
        }
    }

    public static void caso() throws IOException{
        String duracion = br.readLine();
        String[] split = duracion.split(":");

        int horas = Integer.parseInt(split[0]);
        int minutos = Integer.parseInt(split[1]);
        int segundos = Integer.parseInt(split[2]);

        int secondsTotal = (horas * 3600) + (minutos * 60) + segundos;
        int secondsMax = 3600 * 24;
        int rest = secondsMax - secondsTotal;

        horas = rest / 3600;
        rest -= horas * 3600;
        minutos = rest / 60;
        rest -= minutos * 60;
        segundos = rest;

        String h = String.valueOf(horas);
        String m = String.valueOf(minutos);
        String s = String.valueOf(segundos);

        if (horas < 10) h = "0" + horas;
        if (minutos < 10) m = "0" + minutos;
        if (segundos < 10) s = "0" + segundos;

        System.out.println(h + ":" + m + ":" + s);
    }
}

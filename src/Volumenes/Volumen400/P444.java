// 444-La Digestion De Serpientes-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=444&cat=111
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P444 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (!linea.equals("0 0")){
            caso(linea);
            linea = br.readLine();
        }
    }

    public static void caso(String linea) throws IOException {
        int consecutivo = Integer.parseInt(linea.split(" ")[1]);

        String linea2 = br.readLine();
        String[] split = linea2.split(" ");
        int length = split.length;

        int real = 0;
        int tmp = 0;
        boolean counting = false;
        int tmpConsecutivo = 1;
        int max = -1;

        for (int i = 0; i < length; i++) {
            int num = Integer.parseInt(split[i]);

            if (num == 1){
                counting = true;
                tmp++;

                real = tmp;

                tmpConsecutivo = 1;
            } else {
                if (counting){
                    tmp++;
                    if (tmpConsecutivo > consecutivo){
                        if (real > max){
                            max = real;
                        }
                        counting = false;
                        tmp = 0;
                    } else {
                        tmpConsecutivo++;
                    }
                }
            }
        }

        if (real > max){
            max = real;
        }

        System.out.println(max);
    }
}

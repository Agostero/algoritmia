// 462-Mundo Limpio-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=462
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class P462 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        int casos = Integer.parseInt(br.readLine());
        for (int i = 0; i < casos; i++) {
            caso();
        }
    }

    static void caso() throws IOException {
        String linea = br.readLine();
        String[] split = linea.split(" ");

        BigInteger veces = new BigInteger(split[0]);
        String limpias = split[1];
        String[] limpiasSplit = limpias.split(":");
        BigInteger horas = new BigInteger(limpiasSplit[0]);
        BigInteger minutos = new BigInteger(limpiasSplit[1]);
        BigInteger segundos = new BigInteger(limpiasSplit[2]);

        BigInteger segundosTotales = (horas.multiply(new BigInteger("3600"))).add((minutos.multiply(new BigInteger("60"))).add(segundos));
        segundosTotales = segundosTotales.multiply(veces);

        BigInteger dias = segundosTotales.divide(new BigInteger("86400"));
        segundosTotales = segundosTotales.remainder(new BigInteger("86400"));
        horas = segundosTotales.divide(new BigInteger("3600"));
        segundosTotales = segundosTotales.remainder(new BigInteger("3600"));
        minutos = segundosTotales.divide(new BigInteger("60"));
        segundosTotales = segundosTotales.remainder(new BigInteger("60"));
        segundos = segundosTotales;

        String h = String.valueOf(horas);
        if (horas.compareTo(new BigInteger("10")) < 0){
            h = "0" + h;
        }

        String m = String.valueOf(minutos);
        if (minutos.compareTo(new BigInteger("10")) < 0){
            m = "0" + m;
        }

        String s = String.valueOf(segundos);
        if (segundos.compareTo(new BigInteger("10")) < 0){
            s = "0" + s;
        }

        System.out.println(dias + " " + h + ":" + m + ":" + s);
    }
}

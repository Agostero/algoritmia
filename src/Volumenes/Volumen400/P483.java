// 483-Preparando El Reloj-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=483
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P483 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        int casos = Integer.parseInt(br.readLine());
        for (int i = 0; i < casos; i++) {
            caso();
        }
    }

    static void caso() throws IOException {
        String linea = br.readLine();
        String linea2 = br.readLine();

        String[] split = linea.split(" ");
        String[] split2 = linea2.split(" ");

        String horaInicio = split[0];
        String horaFin = split[1];
        int campanadas = Integer.parseInt(split2[0]);
        int campanadaConocer = Integer.parseInt(split2[1]);

        String[] horaInicioSplit = horaInicio.split(":");
        int horaInicioSegundos = (Integer.parseInt(horaInicioSplit[0]) * 3600) + (Integer.parseInt(horaInicioSplit[1]) * 60) + Integer.parseInt(horaInicioSplit[2]);
        String[] horaFinSplit = horaFin.split(":");
        int horaFinSegundos = (Integer.parseInt(horaFinSplit[0]) * 3600) + (Integer.parseInt(horaFinSplit[1]) * 60) + Integer.parseInt(horaFinSplit[2]);

        int diferencia = 0;
        if (horaFinSegundos < horaInicioSegundos){
            diferencia = (86400 - horaInicioSegundos) + horaFinSegundos;
        } else {
            diferencia = horaFinSegundos - horaInicioSegundos;
        }

        int campanadaCada = diferencia / (campanadas - 1);
        int out = (campanadaConocer - 1) * campanadaCada;

        horaInicioSegundos += out;

        if (horaInicioSegundos >= 86400){
            horaInicioSegundos = horaInicioSegundos - 86400;
        }

        int h = horaInicioSegundos/3600;
        horaInicioSegundos %= 3600;
        int m = horaInicioSegundos/60;
        horaInicioSegundos %= 60;
        int s = horaInicioSegundos;

        String hS = String.valueOf(h);
        String mS = String.valueOf(m);
        String sS = String.valueOf(s);

        if (h < 10){
            hS = "0" + hS;
        }

        if (m < 10){
            mS = "0" + mS;
        }

        if (s < 10){
            sS = "0" + sS;
        }

        System.out.println(hS + ":" + mS + ":" + sS);
    }
}

// 477-Captura Doctor Muerte-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=477&cat=117
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class P477 {

    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        int vitalidad = Integer.parseInt(br.readLine());
        while (vitalidad != 0){
            caso(vitalidad);
            vitalidad = Integer.parseInt(br.readLine());
        }
    }

    public static void caso(int vitalidad) throws IOException {
        class Arma implements Comparable<Arma>{
            int villanos;
            int inocentes;
            int numeroInicial;
            boolean usada = false;

            @Override
            public int compareTo(Arma o) {
                if (villanos == o.villanos && inocentes == o.inocentes){
                    return numeroInicial > o.numeroInicial ? 1 : -1;
                }

                if (inocentes == o.inocentes){
                    return villanos > o.villanos ? -1 : 1;
                }

                return inocentes > o.inocentes ? 1: -1;
            }
        }

        int numArmas = Integer.parseInt(br.readLine());
        Arma[] armas = new Arma[numArmas];
        for (int i = 0; i < numArmas; i++) {
            String line = br.readLine();
            String[] split = line.split(" ");
            armas[i] = new Arma();
            armas[i].inocentes = Integer.parseInt(split[0]);
            armas[i].villanos = Integer.parseInt(split[1]);
            armas[i].numeroInicial = (i + 1);
        }

        Arrays.sort(armas);
        for (int i = 0; i < numArmas; i++) {
            Arma a = armas[i];

            vitalidad -= a.villanos;
            a.usada = true;
            if (vitalidad <= 0){
                break;
            }
        }

        boolean first = true;
        if (vitalidad <= 0){
            for (int i = 0; i < numArmas; i++) {
                Arma a = armas[i];

                if (a.usada){
                    if (first){
                        System.out.print(a.numeroInicial);
                        first = false;
                    } else {
                        System.out.print(" " + a.numeroInicial);
                    }
                }
            }
            System.out.println();
        } else {
            System.out.println("MUERTE ESCAPA");
        }
    }
}

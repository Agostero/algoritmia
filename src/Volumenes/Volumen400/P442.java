// 442-Camellos, Serpientes y Kebabs-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=442&cat=111
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P442 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (linea != null){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String linea) {
        StringBuilder sb = new StringBuilder();
        String[] split = linea.split(" ");

        String nombre = split[0];
        String tipo = split[1];
        String formaOriginal = "";

        char[] chars = nombre.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            int c = (int) chars[i];
            if (c >= 60 && c <= 90) {
                formaOriginal = "CamelCase";
                break;
            }
        }

        if (formaOriginal.equals("")){
            int kebab = nombre.indexOf("-");
            int snake = nombre.indexOf("_");

            if (kebab == -1 && snake == -1){ // indeterminado pero no CamelCase
                if (tipo.equals("CamelCase")){
                    nombre = nombre.substring(0, 1).toUpperCase() + nombre.substring(1);
                    sb.append(nombre);
                    System.out.println(sb);
                } else {
                    sb.append(nombre);
                    System.out.println(sb);
                }

                return;
            } else if (kebab != -1){
                formaOriginal = "kebab-case";
            } else {
                formaOriginal = "snake_case";
            }
        }

        if (formaOriginal.equals(tipo)){
            sb.append(nombre);
            System.out.println(sb);
            return;
        }

        if (formaOriginal.equals("CamelCase") && tipo.equals("snake_case")){
            camelSnake(nombre);
        } else if (formaOriginal.equals("CamelCase") && tipo.equals("kebab-case")){
            camelKebab(nombre);
        } else if (formaOriginal.equals("snake_case") && tipo.equals("kebab-case")){
            snakeKebab(nombre);
        } else if (formaOriginal.equals("snake_case") && tipo.equals("CamelCase")){
            snakeCamel(nombre);
        } else if (formaOriginal.equals("kebab-case") && tipo.equals("snake_case")){
            kebabSnake(nombre);
        } else if (formaOriginal.equals("kebab-case") && tipo.equals("CamelCase")){
            kebabCamel(nombre);
        }
    }

    static void camelSnake(String nombre){
        String out = "";

        char[] arr = nombre.toCharArray();
        boolean firstCap = true;
        for (int i = 0; i < arr.length; i++) {
            char c = arr[i];
            if ((int) c >= 60 && (int) c <= 90) {
                if (firstCap){
                    firstCap = false;
                    out += String.valueOf(c).toLowerCase();
                } else {
                    out += "_" + String.valueOf(c).toLowerCase();
                }
            } else {
                out += c;
            }
        }

        System.out.println(out);
    }

    static void camelKebab(String nombre){
        String out = "";

        char[] arr = nombre.toCharArray();
        boolean firstCap = true;
        for (int i = 0; i < arr.length; i++) {
            char c = arr[i];
            if ((int) c >= 60 && (int) c <= 90) {
                if (firstCap){
                    firstCap = false;
                    out += String.valueOf(c).toLowerCase();
                } else {
                    out += "-" + String.valueOf(c).toLowerCase();
                }
            } else {
                out += c;
            }
        }

        System.out.println(out);
    }

    static void snakeKebab(String nombre){
        nombre = nombre.replace("_", "-");
        System.out.println(nombre);
    }

    static void snakeCamel(String nombre){
        String out = "";

        char[] arr = nombre.toCharArray();
        boolean upper = true;
        for (int i = 0; i < arr.length; i++) {
            char c = arr[i];
            if (upper){
                upper = false;
                out += String.valueOf(c).toUpperCase();
            } else {
                if (c == '_'){
                    upper = true;
                } else {
                    out += c;
                }
            }
        }

        System.out.println(out);
    }

    static void kebabSnake(String nombre){
        nombre = nombre.replace("-", "_");
        System.out.println(nombre);
    }

    static void kebabCamel(String nombre){
        String out = "";

        char[] arr = nombre.toCharArray();
        boolean upper = true;
        for (int i = 0; i < arr.length; i++) {
            char c = arr[i];
            if (upper){
                upper = false;
                out += String.valueOf(c).toUpperCase();
            } else {
                if (c == '-'){
                    upper = true;
                } else {
                    out += c;
                }
            }
        }

        System.out.println(out);
    }
}

// 439-Velocidad Desplazamiento Tiempo-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=439&cat=111
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P439 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        int casos = Integer.parseInt(br.readLine());
        for (int i = 0; i < casos; i++) {
            caso();
        }
    }

    public static void caso() throws IOException {
        String linea = br.readLine();
        String[] split = linea.split(" ");

        String first = split[0];
        String[] firstSplit = first.split("=");
        String letra1 = firstSplit[0];
        int numero1 = Integer.parseInt(firstSplit[1]);

        String second = split[1];
        String[] secondSplit = second.split("=");
        String letra2 = secondSplit[0];
        int numero2 = Integer.parseInt(secondSplit[1]);

        String outLetra = "";
        int outValor = 0;

        if (letra1.equals("D") && letra2.equals("T")){
            outLetra = "V";
            outValor = numero1 / numero2;

        } else if (letra1.equals("D") && letra2.equals("V")){
            outLetra = "T";
            outValor = numero1 / numero2;

        } else if (letra1.equals("V") && letra2.equals("T")){
            outLetra = "D";
            outValor = numero1 * numero2;

        } else if (letra1.equals("V") && letra2.equals("D")){
            outLetra = "T";
            outValor = numero2 / numero1;

        } else if (letra1.equals("T") && letra2.equals("D")){
            outLetra = "V";
            outValor = numero2 / numero1;

        } else if (letra1.equals("T") && letra2.equals("V")){
            outLetra = "D";
            outValor = numero1 * numero2;
        }

        System.out.println(outLetra + "=" + outValor);
    }
}

// 488-Me Duermo En Los Juegos-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=488
package Volumenes.Volumen400;


import java.io.*;

public class P488 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        int casos = Integer.parseInt(br.readLine());
        for (int i = 0; i < casos; i++) {
            caso();
        }
    }

    static void caso() throws IOException{
        String linea = br.readLine();
        String[] split = linea.split(" ");
        int puntos = Integer.parseInt(split[0]);
        int minutos = Integer.parseInt(split[1]);

        String linea2 = br.readLine();
        String[] arr = linea2.split(" ");

        int ref = minutos;
        int sesiones = 1;
        int anterior = 0;
        for (int i = 0; i < puntos; i++) {
            int n = Integer.parseInt(arr[i]);

            if (ref < n){
                sesiones++;
                ref = anterior + minutos;
                i--;
            } else {
                anterior = n;
            }
        }

        System.out.println(sesiones);
    }
}

// 434-Romance En El Palomar-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=434&cat=111
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P434 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        int casos = Integer.parseInt(br.readLine());

        for (int i = 0; i < casos; i++) {
            caso();
        }
    }

    public static void caso() throws IOException {
        String linea = br.readLine();
        String[] split = linea.split(" ");

        int palomas = Integer.parseInt(split[0]);
        int cajas = Integer.parseInt(split[1]);

        if (palomas <= cajas){
            System.out.println("ROMANCE");
        } else if (palomas > cajas){
            System.out.println("PRINCIPIO");
        }
    }
}

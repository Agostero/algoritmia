// 476-Lucha De Egos-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=476&cat=117
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P476 {
    public static void main(String[] args) throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        // Clase para almacenar la destreza del heroe y el minimo y maximo numero al que se puede enfrentar este heroe.
        // Tambien guardo la variable asignado que me permitira determinar si un heroe ya ha sido emparejado o no
        class Heroe {
            int destreza;
            int minimo;
            int maximo;
            boolean asignado = false;
        }

        // obtengo la cantidad de heroes
        int numHeroes = Integer.parseInt(br.readLine());

        // mientras heroes no sea 0, continuo analizando casos
        while (numHeroes != 0){
            // creo una lista de heroes del tamaño dado
            Heroe[] heroes = new Heroe[numHeroes];
            // obtengo la linea y la separo por espacio, lo que me dara las destrezas de los heroes
            String linea = br.readLine();
            String[] lineaDestreza = linea.split(" ");
            // tambien obtengo la segunda linea que la separo por espacio y me dara la preferencia del heroe
            String linea2 = br.readLine();
            String[] lineaPreferencia = linea2.split(" ");
            for (int i = 0; i < lineaDestreza.length; i++) {
                // añado el heroe a la lista con su destreza
                heroes[i] = new Heroe();
                heroes[i].destreza = Integer.parseInt(lineaDestreza[i]);

                //obtengo el simbolo y el numero de la preferencia
                String[] preferencia = lineaPreferencia[i].split("(?!^)");
                String simbolo = preferencia[0];
                int numero = Integer.parseInt(preferencia[1]);

                // si el simbolo es =, entonces el minimo y maximo de preferencia de este heroe sera el mismo numero obtenido
                // si el simbolo es >, entonces el minimo sera el numero dado por pantalla mas 1 porque no es inclusivo y el numero maximo sera un 5 porque es el nivel maximo de estreza
                // si el simbolo es <, entonces el maximo sera el numero dado por pantalla menos 1 porque tampoco es inclusivo y el minimo sera 1 porque es el nivel minimo de destreza posible
                if (simbolo.equals("=")){
                    heroes[i].minimo = numero;
                    heroes[i].maximo = numero;
                } else if (simbolo.equals(">")){
                    heroes[i].minimo = numero + 1;
                    heroes[i].maximo = 5;
                } else if (simbolo.equals("<")){
                    heroes[i].maximo = numero - 1;
                    heroes[i].minimo = 1;
                }
            }

            // guardo una variable que me permitira determinar si he encontrado emparejamientos o no
            boolean found = false;
            for (int i = 0; i < numHeroes; i++) {
                Heroe h1 = heroes[i];
                for (int j = 0; j < numHeroes; j++) {
                    // por cada heroe, analizo un heroe que no sea el mismo
                    /**
                     * AQUI HAY DEBATE. El enunciado dice "La asignación de emparejamientos se realiza buscando desde el primero hasta el último el primer superhéroe que vaya después en la lista que permita que ambos estén conformes con el enfrentamiento."
                     * POR LO TANTO: la condicion que sigue despues de este comentario podria ser (j != 1) para emparejar a cualquier heroe con cualquier otro aunque este ante antes en la lista o podria ser (j > i) para buscar solo heroes que esten despues del analizado
                     * No acabo de entender bien el enunciado. De cualquier forma, he probado las dos condiciones y sigue dando RUN TIME ERROR y debería dar WRONG ANSWER si fuera este el problema
                     * Y ADEMAS ES INSTANTANEO EL RUN TIME ERROR, NO ANALIZA NADA EL JUEZ
                     * **/
                    if (j > i){
                        Heroe h2 = heroes[j];
                        // compruebo si la destreza del segundo esta dentro del margen de minimo y maximo del primero y viceversa para determinar que los dos pueden estar emparejados
                        if (h2.destreza >= h1.minimo && h2.destreza <= h1.maximo && h1.destreza >= h2.minimo && h1.destreza <= h2.maximo && !h1.asignado && !h2.asignado){
                            // imprimo la posicion real que ocupan los dos heroes y les pongo asignado a true para que no vuelvan a ser emparejados
                            System.out.println((i + 1) + " " + (j + 1));
                            h1.asignado = true;
                            h2.asignado = true;
                            found = true;
                        }
                    }
                }
            }

            // Si anteriormente no encontre algun emparejamiento imprimo "NO HAY"
            if (!found){
                System.out.println("NO HAY");
            }

            // Imprimo --- para determinar que el caso se ha procesado
            System.out.println("---");

            // Pido numero de heroes de nuevo
            numHeroes = Integer.parseInt(br.readLine());
        }
    }
}

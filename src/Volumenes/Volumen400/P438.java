// 438-Esgritura-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=438&cat=111
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P438 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        while(true){
            String linea = br.readLine();
            if (linea == null){
                break;
            }

            char[] arr = linea.toCharArray();
            int countSignos = 0, countLetras = 0;
            int length = arr.length;
            for (int i = 0; i < length; i++) {
                if (arr[i] == '!'){
                    countSignos++;
                } else {
                    int c = arr[i];

                    if ((c >= 97 && c <= 122) || (c >= 65 && c <= 90)){
                        countLetras++;
                    }
                }
            }

            if (countSignos > countLetras){
                System.out.println("ESGRITO");
            } else {
                System.out.println("escrito");
            }
        }
    }
}

// 485-Senda Pirenaica-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=485
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P485 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (!linea.equals("0")){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String nEtapas) throws IOException {
        int etapas = Integer.parseInt(nEtapas);
        String linea = br.readLine();
        int[] out = new int[etapas];

        int total = 0;
        String[] split = linea.split(" ");
        int l = split.length;
        for (int i = l - 1; i >= 0; i--) {
            int n = Integer.parseInt(split[i]);
            total += n;
            out[i] = total;
        }


        String result = "";
        for (int i = 0; i < l; i++) {
            if (i == l - 1){
                result += out[i];
            } else {
                result += out[i] + " ";
            }
        }
        System.out.println(result);
    }
}

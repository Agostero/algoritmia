// 489-Nos Mudamos-Time Limit Exceeded-https://www.aceptaelreto.com/problem/statement.php?id=489
package Volumenes.Volumen400;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class P489 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (linea != null && !linea.equals("")){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String linea1) throws IOException{
        String linea2 = br.readLine();
        String linea3 = br.readLine();

        if (linea2.length() > linea3.length()){
            System.out.println("NO");
            return;
        }

        String[] linea2split = linea2.split(" ");
        String[] linea3split = linea3.split(" ");

        Arrays.sort(linea2split);
        Arrays.sort(linea3split);

        int l1 = linea2split.length;
        int l2 = linea3split.length;
        int index = 0;
        for (int i = 0; i < l1; i++) {
            int n1 = Integer.parseInt(linea2split[i]);

            boolean fit = false;
            for (int j = index; j < l2; j++) {
                int n2 = Integer.parseInt(linea3split[j]);

                if (n2 - n1 >= 0){
                    fit = true;
                    index = j + 1;
                    break;
                }
            }

            if (!fit){
                System.out.println("NO");
                return;
            }
        }

        System.gc();
        System.out.println("SI");
    }




}

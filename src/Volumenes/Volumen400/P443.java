// 443-Abanico De Naipes-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=443&cat=111
package Volumenes.Volumen400;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

public class P443 {
    static class Reader
    {
        final private int BUFFER_SIZE = 1 << 16;
        private DataInputStream din;
        private byte[] buffer;
        private int bufferPointer, bytesRead;

        public Reader()
        {
            din = new DataInputStream(System.in);
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }

        public Reader(String file_name) throws IOException
        {
            din = new DataInputStream(new FileInputStream(file_name));
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }

        public String readLine(int length) throws IOException
        {
            byte[] buf = new byte[length]; // line length
            int cnt = 0, c;
            while ((c = read()) != -1)
            {
                if (c == '\n')
                    break;
                buf[cnt++] = (byte) c;
            }
            return new String(buf, 0, cnt);
        }

        public int nextInt() throws IOException
        {
            int ret = 0;
            byte c = read();
            while (c <= ' ')
                c = read();
            boolean neg = (c == '-');
            if (neg)
                c = read();
            do
            {
                ret = ret * 10 + c - '0';
            }  while ((c = read()) >= '0' && c <= '9');

            if (neg)
                return -ret;
            return ret;
        }

        public long nextLong() throws IOException
        {
            long ret = 0;
            byte c = read();
            while (c <= ' ')
                c = read();
            boolean neg = (c == '-');
            if (neg)
                c = read();
            do {
                ret = ret * 10 + c - '0';
            }
            while ((c = read()) >= '0' && c <= '9');
            if (neg)
                return -ret;
            return ret;
        }

        public double nextDouble() throws IOException
        {
            double ret = 0, div = 1;
            byte c = read();
            while (c <= ' ')
                c = read();
            boolean neg = (c == '-');
            if (neg)
                c = read();

            do {
                ret = ret * 10 + c - '0';
            }
            while ((c = read()) >= '0' && c <= '9');

            if (c == '.')
            {
                while ((c = read()) >= '0' && c <= '9')
                {
                    ret += (c - '0') / (div *= 10);
                }
            }

            if (neg)
                return -ret;
            return ret;
        }

        private void fillBuffer() throws IOException
        {
            bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
            if (bytesRead == -1)
                buffer[0] = -1;
        }

        private byte read() throws IOException
        {
            if (bufferPointer == bytesRead)
                fillBuffer();
            return buffer[bufferPointer++];
        }

        public void close() throws IOException
        {
            if (din == null)
                return;
            din.close();
        }
    }

    static Reader sc;

    public static void main(String[] args) throws IOException {
        sc = new Reader();

        int numCartas = sc.nextInt();
        while (numCartas != 0){
            caso(numCartas);
            numCartas = sc.nextInt();
        }
    }

    static void caso(int numCartas) throws IOException {

        class Carta implements Comparable<Carta> {
            int num;
            int index;

            public Carta(int num, int index){
                this.num = num;
                this.index = index;
            }

            @Override
            public int compareTo(Carta o) {
                if (num > o.num){
                    return 1;
                } else {
                    return -1;
                }
            }

            @Override
            public String toString() {
                return "Carta{" +
                        "num=" + num +
                        ", index=" + index +
                        '}';
            }
        }

        Carta[] arr = new Carta[numCartas];
        for (int i = 0; i < numCartas; i++) {
            int n = sc.nextInt();
            arr[i] = new Carta(n, i);
        }

        Arrays.sort(arr);

        int total = 1;
        Carta previous = arr[0];
        for (int i = 1; i < numCartas; i++) {
            Carta c = arr[i];

            if (c.index < previous.index){
                break;
            } else {
                total++;
            }

            previous = c;
        }

        System.out.println(numCartas - total);
    }
}

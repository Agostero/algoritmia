// 433-Racimos De Uvas-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=433&cat=111
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P433 {

    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        int numUvas = Integer.parseInt(br.readLine());
        while (numUvas != 0){
            caso(numUvas);
            numUvas = Integer.parseInt(br.readLine());
        }
    }

    public static void caso(int numUvas){
        int out = 0;

        int index = 1;
        while (numUvas > 0){
            numUvas -= index;
            out++;
            index++;
        }

        System.out.println(out);
    }
}

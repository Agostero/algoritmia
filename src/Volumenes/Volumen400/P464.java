// 464-Entrando En Peloton-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=464&cat=110
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class P464 {

    static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (linea != null){
            caso(Integer.parseInt(linea));
            linea = br.readLine();
        }
    }

    static void caso(int numeroCiclistas) throws IOException{
        class Ciclista implements Comparable<Ciclista>{
            int indice;
            int tiempo;

            public Ciclista(int indice, int tiempo){
                this.indice = indice;
                this.tiempo = tiempo;
            }

            @Override
            public int compareTo(Ciclista o) {
                if (tiempo == o.tiempo){
                    return 0;
                } else if (tiempo > o.tiempo){
                    return 1;
                } else {
                    return -1;
                }
            }

            @Override
            public String toString() {
                return "Ciclista{" +
                        "indice=" + indice +
                        ", tiempo=" + tiempo +
                        '}';
            }
        }

        Ciclista[] ciclistas = new Ciclista[numeroCiclistas];
        for (int i = 0; i < numeroCiclistas; i++) {
            String line = br.readLine();
            int seconds = getSeconds(line);
            Ciclista c = new Ciclista(i, seconds);
            ciclistas[i] = c;
        }

        Arrays.sort(ciclistas);

        int[] output = new int[numeroCiclistas];
        int numeroTotal = 1;
        int numeroActual = 1;
        output[ciclistas[0].indice] = 1;
        for (int i = 0; i < numeroCiclistas - 1; i++) {
            Ciclista actual = ciclistas[i];
            Ciclista siguiente = ciclistas[i + 1];
            numeroTotal++;
            if (siguiente.tiempo - actual.tiempo <= 1){
                output[siguiente.indice] = numeroActual;
            } else {
                numeroActual = numeroTotal;
                output[siguiente.indice] = numeroActual;
            }
        }

        for (int i = 0; i < output.length; i++) {
            System.out.println(output[i]);
        }
        System.out.println("---");
    }

    public static int getSeconds(String date){
        int output = 0;

        // ESTO PETA NO SE POR QUE

        String[] dateSplit = date.split(":");
        output += ((Integer.parseInt(dateSplit[0]) * 3600));
        output += ((Integer.parseInt(dateSplit[1]) * 60));
        output += ((Integer.parseInt(dateSplit[2])));

        return output;
    }
}

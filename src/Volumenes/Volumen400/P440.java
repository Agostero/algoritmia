// 440-Escalando El Everest-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=440&cat=111
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P440 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        int escaladores = Integer.parseInt(br.readLine());
        while (escaladores != 0){
            caso(escaladores);
            escaladores = Integer.parseInt(br.readLine());
        }
    }

    public static void caso(int escaladores) throws IOException {
        String linea = br.readLine();
        String[] split = linea.split(" ");
        int length = split.length;

        int grupos = 1;
        int max = 0;
        int min = Integer.MAX_VALUE;
        int grupoActual = 1;
        int currentSpeed = Integer.parseInt(split[0]);
        for (int i = 1; i < length; i++) {
            int num = Integer.parseInt(split[i]);

            if (num < currentSpeed){
                if (grupoActual > max){
                    max = grupoActual;
                }

                if (grupoActual < min){
                    min = grupoActual;
                }

                currentSpeed = num;
                grupoActual = 1;
                grupos++;
            } else {
                grupoActual++;
            }
        }

        if (grupoActual > max){
            max = grupoActual;
        }

        if (grupoActual < min){
            min = grupoActual;
        }

        System.out.println(grupos + " " + min + " " + max);

    }
}

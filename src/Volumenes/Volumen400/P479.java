// 479-El Hombre Sin Miedo-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=479&cat=117
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P479 {

    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        // pido primera linea y la separo en los dos numeros
        String linea = br.readLine();
        String[] split = linea.split(" ");
        int filas = Integer.parseInt(split[0]);
        int columnas = Integer.parseInt(split[1]);

        // mientras la linea no sea 0 0 entonces realiza un caso
        while (!linea.equalsIgnoreCase("0 0")){
            // procesamiento del caso
            caso(filas, columnas);

            // una vez procesado, pedir nueva entrada
            linea = br.readLine();
            split = linea.split(" ");
            filas = Integer.parseInt(split[0]);
            columnas = Integer.parseInt(split[1]);
        }
    }

    public static void caso(int filas, int columnas) throws IOException{
        // creacion de la sala
        String[][] sala = new String[filas][columnas];

        // por cada fila pido una linea y la separo por strings de un caracter
        for (int i = 0; i < filas; i++) {
            String linea = br.readLine();
            String[] split = linea.split("(?!^)");
            // por cada caracter lo voy introduciendo en la matriz
            for (int j = 0; j < columnas; j++) {
                sala[i][j] = split[j];
            }
        }

        // obtengo la cantidad de consultas que se realizaran por pantalla
        int numConsultas = Integer.parseInt(br.readLine());
        // por cada consulta la separo en el primer numero, el segundo y la direccion
        for (int i = 0; i < numConsultas; i++) {
            String linea = br.readLine();
            String[] split = linea.split(" ");

            int fila = (Integer.parseInt(split[0]) - 1);
            int columna = (Integer.parseInt(split[1]) - 1);
            String direccion = split[2];

            // calculo la distancia a la X mas cercana en la matriz en base a esta informacion
            calcularDistancia(sala, fila, columna, direccion);
        }
        // imprimo --- para determinar que el caso ha finalizado
        System.out.println("---");
    }

    public static void calcularDistancia(String[][] sala, int fila, int columna, String direccion){
        boolean found = false;

        int distancia = 0;
        // mientras no haya encontrado la X, continuar el bucle
        while (!found){
            // dependiendo de la direccion transformo la fila o la columna en la direccion correcta
            switch (direccion){
                case "ARRIBA": {
                    fila--;
                    break;
                }
                case "DERECHA": {
                    columna++;
                    break;
                }
                case "ABAJO": {
                    fila++;
                    break;
                }
                case "IZQUIERDA": {
                    columna--;
                    break;
                }
            }
            // sumo uno a la distancia porque voy a moverme
            distancia++;

            // con positionExists compruebo que la fila y la columna existen dentro de la matriz. Si no existe, salgo del bucle
            if (positionExists(sala, fila, columna)){
                // obtengo el caracter que estoy analizando
                String kar = sala[fila][columna];
                // si es una X entonces lo he encontrado
                if (kar.equalsIgnoreCase("X")){
                    found = true;
                }
            } else {
                break;
            }
        }

        // despues de salir del bucle, compruebo si en algun momento encontre la X o no. Si no la encontre imprimo NINGUNO y si la encontre imprimo la cantidad de pasos que he dado para encontrarla
        if (found){
            System.out.println(distancia);
        } else {
            System.out.println("NINGUNO");
        }
    }

    public static boolean positionExists(String[][] sala, int fila, int columna){
        boolean out = false;

        if (fila >= 0 && fila < sala.length && columna >= 0 && columna < sala[0].length){
            out = true;
        }

        return out;
    }
}

// 412-Ovejas Negras-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=412
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P412 {
    public static BufferedReader br;
    public static char[][] matriz;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (linea != null){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String linea) throws IOException {
        String[] split = linea.split(" ");
        int n1 = Integer.parseInt(split[0]);
        int n2 = Integer.parseInt(split[1]);

        matriz = new char[n2][n1];

        for (int i = 0; i < n2; i++) {
            char[] l = br.readLine().toCharArray();
            for (int j = 0; j < n1; j++) {
                matriz[i][j] = l[j];
            }
        }

        fillBlanks(0 , 0);

        for (int i = 0; i < n2; i++) {
            for (int j = 0; j < n1; j++) {
                if (matriz[i][j] == '.'){
                    System.out.println("SI");
                    return;
                }
            }
        }

        System.out.println("NO");
    }

    static void fillBlanks(int x, int y){
        if (x >= 0 && x < matriz.length && y >= 0 && y < matriz[0].length){
            char c = matriz[x][y];

            if (c == '.'){
                matriz[x][y] = '/';

                fillBlanks(x + 1, y);
                fillBlanks(x - 1, y);
                fillBlanks(x, y + 1);
                fillBlanks(x, y - 1);
            }
        }
    }
}

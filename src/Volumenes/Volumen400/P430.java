// 430-Las Pruebas Del Maestro Joda-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=430&cat=108
package Volumenes.Volumen400;

import java.util.Scanner;

public class P430 {

    public static class Prueba {
        int dentroDe;
        int cada;

        public Prueba(int dentrode, int cada){
            this.dentroDe = dentrode;
            this.cada = cada;
        }

        public int calcularProximoDia(int diaActual){
            if (diaActual < dentroDe){
                return dentroDe - diaActual;
            } else {
                int temp = dentroDe;

                while (true){
                    temp += cada;
                    if (temp > diaActual){
                        return temp - diaActual;
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (true){
            int p = sc.nextInt();

            if (p == 0){
                break;
            }

            int diaActual = 0;
            Prueba[] pruebas = new Prueba[p];
            for (int i = 0; i < p; i++) {
                int d = sc.nextInt();
                int c = sc.nextInt();

                pruebas[i] = new Prueba(d, c);
                int diasDeMas = pruebas[i].calcularProximoDia(diaActual);

                diaActual += diasDeMas;
            }

            System.out.println(diaActual);
        }
    }
}

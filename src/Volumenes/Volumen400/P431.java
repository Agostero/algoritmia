// 431-Genetica Jedi-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=431&cat=108
package Volumenes.Volumen400;

import java.util.Arrays;
import java.util.Scanner;

public class P431 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while(true){
            int numFamilias = sc.nextInt();

            if (numFamilias == 0){
                break;
            }

            int[] output = new int[numFamilias];
            for (int i = 0; i < numFamilias; i++) {
                int numHijos = sc.nextInt();
                int midiclorianos = sc.nextInt();

                int max = Integer.MIN_VALUE;
                for (int j = 0; j < numHijos; j++) {
                    int hijo = sc.nextInt() * midiclorianos;
                    if (hijo > max){
                        max = hijo;
                    }
                }

                output[i] = max;
            }

            Arrays.sort(output);

            for (int i = output.length - 1; i >= 0; i--) {
                if (i == 0){
                    System.out.print(output[i]);
                } else {
                    System.out.print(output[i] + " ");
                }
            }
            System.out.println();
        }
    }
}

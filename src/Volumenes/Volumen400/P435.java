// 435-El Pijote-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=435&cat=111
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P435 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        while (true){
            String linea = br.readLine();
            if (linea == null){
                break;
            }

            char[] arr = linea.toCharArray();

            int n0 = 0, n1 = 0, n2 = 0, n3 = 0, n4 = 0, n5 = 0, n6 = 0, n7 = 0, n8 = 0, n9 = 0;
            int length = arr.length;
            for (int i = 0; i < length; i++) {
                char c = arr[i];
                if (c == '0') n0++;
                else if (c == '1') n1++;
                else if (c == '2') n2++;
                else if (c == '3') n3++;
                else if (c == '4') n4++;
                else if (c == '5') n5++;
                else if (c == '6') n6++;
                else if (c == '7') n7++;
                else if (c == '8') n8++;
                else if (c == '9') n9++;
            }

            if (n0 == n1 && n1 == n2 && n2 == n3 && n3 == n4 && n4 == n5 && n5 == n6 && n6 == n7 && n7 == n8 && n8 == n9){
                System.out.println("subnormal");
            } else {
                System.out.println("no subnormal");
            }
        }
    }
}

// 490-Turismo De Luces-Memory Limit Exceeded-https://www.aceptaelreto.com/problem/statement.php?id=490
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P490 {

    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (linea != null){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String linea) throws IOException {
        String[] split = linea.split(" ");
        // int total = Integer.parseInt(split[0]);
        int aRecorrer = Integer.parseInt(split[1]);

        String nuevaLinea = br.readLine();
        String[] splitNueva = nuevaLinea.split(" ");

        int max = Integer.MIN_VALUE;
        int index = 0;
        while(true){
            int previo = Integer.parseInt(splitNueva[index]);
            int sum = 0;
            int sum2 = 0;
            for (int i = index + 1; i < index + aRecorrer; i++) {
                int siguiente = Integer.parseInt(splitNueva[i]);

                int resultado = previo - siguiente;

                sum += resultado;
                sum2 += (-resultado);

                previo = siguiente;
            }

            if (sum > max){
                max = sum;
            }

            if (sum2 > max){
                max = sum2;
            }

            index++;
            int restante = splitNueva.length - index;
            if (restante < aRecorrer){
                break;
            }
        }

        System.out.println(max);
    }
}

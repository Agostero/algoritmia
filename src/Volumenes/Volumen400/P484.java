// 484-El Incidente De Dhahran-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=484
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P484 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (linea != null){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String numero) {
        String[] split = numero.split("\\.");

        if (split.length > 1){
            String n1 = split[0];
            String n2 = split[1];

            String s1 = removeFrontZeros(n1);
            String s2 = removeTrailingZeros(n2);

            if (s2.equals("0")){
                System.out.println(s1);
            } else {
                System.out.println(s1 + "." + s2);
            }
        } else {
            String n1 = split[0];

            String s1 = removeFrontZeros(n1);
            System.out.println(s1);
        }
    }

    static String removeFrontZeros(String n){
        String[] split = n.split("(?!^)");
        int l = split.length;

        String out = "";
        boolean found = false;
        for (int i = 0; i < l; i++) {
            String t = split[i];
            if (!found){
                if (!t.equals("0")){
                    out = out + t;
                    found = true;
                }
            } else {
                out = out + t;
            }
        }

        if (out.equals("")){
            return "0";
        } else {
            return out;
        }
    }

    static String removeTrailingZeros(String n){
        String[] split = n.split("(?!^)");
        int l = split.length;

        String out = "";
        boolean found = false;
        for (int i = l - 1; i >= 0; i--) {
            String t = split[i];
            if (!found){
                if (!t.equals("0")){
                    out = t + out;
                    found = true;
                }
            } else {
                out = t + out;
            }
        }

        if (out.equals("")){
            return "0";
        } else {
            return out;
        }
    }
}

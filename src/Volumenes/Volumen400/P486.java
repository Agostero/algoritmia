// 486-Hay Que Compartir-Wrong Answer-https://www.aceptaelreto.com/problem/statement.php?id=485
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P486 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (!linea.equals("0")){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String macarons) throws IOException{
        int nMacarons = Integer.parseInt(macarons);

        String linea = br.readLine();
        String[] split = linea.split(" ");
        int l = split.length;

        int[] arriba = new int[nMacarons];
        int[] abajo = new int[nMacarons];

        int max = Integer.MIN_VALUE;
        for (int i = 0; i < l; i++) {
            String f = split[i];
            String[] splitF = f.split("/");
            int n1 = Integer.parseInt(splitF[0]);
            int n2 = Integer.parseInt(splitF[1]);

            if (n2 > max){
                max = n2;
            }

            arriba[i] = n1;
            abajo[i] = n2;
        }

        int index = 1;
        /*while(true){
            int multiple = max * index;
            boolean found = true;
            for (int i = 0; i < nMacarons; i++) {
                if (multiple % abajo[i] != 0){
                    found = false;
                }
            }

            if (found){
                break;
            } else {
                index++;
            }
        }*/

        index = Integer.parseInt(macarons);

        int multiplicador = index * max;
        int n1 = 0;
        int n2 = multiplicador;
        for (int i = 0; i < nMacarons; i++) {
            int ar = arriba[i];
            int ab = abajo[i];

            int result = multiplicador / ab;
            ar *= result;
            n1 += ar;
        }

        // resultado =  n1/n2
        //              x /n2

        int x = (multiplicador * (n2 - n1)) / multiplicador;
        System.out.println(x);
    }
}

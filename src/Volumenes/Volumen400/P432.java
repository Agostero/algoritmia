// 432-Escapando De Las Fuerzas Imperiales-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=432&cat=108
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P432 {

    static BufferedReader sc;
    static char[][] matriz;
    static boolean found = false;

    public static void main(String[] args) throws IOException {

        sc = new BufferedReader(new InputStreamReader(System.in));
        String linea = sc.readLine();
        while (linea != null){
            caso(linea);
            linea = sc.readLine();
        }
    }

    public static void caso(String linea) throws IOException {
        String[] split = linea.split(" ");
        int filas = Integer.parseInt(split[0]);
        int columnas = Integer.parseInt(split[1]);

        int filaInicial = 0;
        int columnaInicial = 0;

        matriz = new char[filas][columnas];
        for (int i = 0; i < filas; i++) {
            char[] l = sc.readLine().toCharArray();
            for (int j = 0; j < columnas; j++) {
                char c = l[j];
                if (c == 'S'){
                    filaInicial = i;
                    columnaInicial = j;
                }
                matriz[i][j] = c;
            }
        }

        findF(filaInicial, columnaInicial);

        if (found){
            System.out.println("SI");
        } else {
            System.out.println("NO");
        }

        found = false;
    }

    static void findF(int x, int y){
        if (x >= 0 && x < matriz.length && y >= 0 && y < matriz[0].length){
            char c = matriz[x][y];

            if (c == '.' || c == 'S'){
                matriz[x][y] = '/';

                findF(x + 1, y);
                findF(x - 1, y);
                findF(x, y + 1);
                findF(x, y - 1);
            } else if (c == 'F'){
                found = true;
            }
        }
    }
}

// 427-Yo Soy Tu-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=427&cat=108
package Volumenes.Volumen400;

import java.util.Scanner;

public class P427 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int casos = sc.nextInt();
        sc.nextLine();

        for (int i = 0; i < casos; i++) {
            String nombre = sc.nextLine();
            String parentesco = sc.nextLine();

            if (nombre.equals("Luke") && parentesco.equals("padre")){
                System.out.println("TOP SECRET");
            } else {
                System.out.println(nombre + ", yo soy tu " + parentesco);
            }
        }
    }
}

// 482-Los Calcetines De Ian Malcolm-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=482
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P482 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (!linea.equals(".")){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String linea){
        String[] split = linea.split(" ");
        int n = 0;
        int g = 0;

        int l = split.length;
        for (int i = 0; i < l; i++) {
            String d = split[i];
            if (d.equals("N")){
                n++;
            } else if (d.equals("G")){
                g++;
            }
        }

        if (n % 2 == 0 && g % 2 == 0){
            System.out.println("EMPAREJADOS");
        } else if (n % 2 != 0 && g % 2 != 0){
            System.out.println("PAREJA MIXTA");
        } else if (n % 2 == 0 && g % 2 != 0){
            System.out.println("GRIS SOLITARIO");
        } else if (n % 2 != 0 && g % 2 == 0){
            System.out.println("NEGRO SOLITARIO");
        }
    }
}

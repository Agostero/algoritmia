// 436-Reto Superado-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=436&cat=111
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P436 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        while (true){
            String linea = br.readLine();
            if (linea == null){
                break;
            }

            String[] split = linea.split(" ");
            int grosor = Integer.parseInt(split[0]);
            int metros = Integer.parseInt(split[1]);
            double grosorMicras = grosor * 0.000001;
            int veces = 0;

            while (grosorMicras < metros){
                grosorMicras *= 2;
                veces++;
            }

            System.out.println(veces);
        }
    }
}

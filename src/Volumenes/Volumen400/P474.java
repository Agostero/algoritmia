// 474-Tu Amigable Vecino-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=474&cat=117
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P474 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int casos = Integer.parseInt(br.readLine());

        for (int i = 0; i < casos; i++) {
            String[] linea = br.readLine().split(" ");
            int posicion = Integer.parseInt(linea[0]);

            int bombaA = Integer.parseInt(linea[1]);
            int bombaB = Integer.parseInt(linea[2]);

            int distanciaBombaA = Math.abs(posicion - bombaA);
            int distanciaBombaB = Math.abs(posicion - bombaB);

            int total = 0;
            if (distanciaBombaA <= distanciaBombaB){
                total += distanciaBombaA;
                total += Math.abs(bombaB - bombaA);
            } else {
                total += distanciaBombaB;
                total += Math.abs(bombaB - bombaA);
            }

            System.out.println(total);
        }
    }
}

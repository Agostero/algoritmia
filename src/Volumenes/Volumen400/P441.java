// 441-Contar Hasta El Final-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=441&cat=111
package Volumenes.Volumen400;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P441 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (linea != null){
            caso(linea);
            linea = br.readLine();
        }
    }

    public static void caso(String linea){
        String[] split = linea.split("\\.");
        int length = split.length;

        StringBuilder sb = new StringBuilder();

        boolean firstTime = true;
        boolean getOne = false;
        for (int i = length - 1; i >= 0; i--) {
            int num = Integer.parseInt(split[i]);

            if (firstTime){
                firstTime = false;
                num++;
            }

            if (getOne){
                num++;
                getOne = false;
            }

            if (i != 0){
                if (num >= 1000){
                    getOne = true;
                    sb.insert(0, "000");
                } else {
                    if (num < 10){
                        sb.insert(0, "00" + num);
                    } else if (num < 100){
                        sb.insert(0, "0" +  num);
                    } else {
                        sb.insert(0, num);
                    }
                }
                sb.insert(0, ".");
            } else {
                if (num >= 1000){
                    sb.insert(0, "1.000");
                } else {
                    sb.insert(0, num);
                }
            }
        }

        System.out.println(sb);
    }
}

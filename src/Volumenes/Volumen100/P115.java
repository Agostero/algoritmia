// 115-El Numero De Kaprekar-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=115
package Volumenes.Volumen100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P115 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (!linea.equals("0")){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String linea){
        long objetivo = Long.parseLong(linea);
        String analizar = "0" + String.valueOf(objetivo * objetivo);
        String[] splitAnalizar = analizar.split("(?!^)");
        int length = analizar.length();

        String n1 = "";
        String n2 = "";

        for (int i = 0; i < length; i++) {
            n1 += splitAnalizar[i];
            for (int j = i + 1; j < length; j++) {
                n2 += splitAnalizar[j];
            }

            if (n2.length() > 0 && Long.parseLong(n2) != 0 && Long.parseLong(n1) + Long.parseLong(n2) == objetivo){
                System.out.println("SI");
                return;
            }

            n2 = "";
        }

        System.out.println("NO");
    }
}

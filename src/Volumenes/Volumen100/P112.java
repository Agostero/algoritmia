// 112-Radares De Tramo-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=112
package Volumenes.Volumen100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P112 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (!linea.equals("0 0 0")){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String linea){
        String[] split = linea.split(" ");
        double distancia = Integer.parseInt(split[0]);
        double maxima = Integer.parseInt(split[1]);
        double segundos = Integer.parseInt(split[2]);

        double maxima20 = maxima + (maxima * 0.2);

        if (distancia <= 0 || maxima <= 0 || segundos <= 0){
            System.out.println("ERROR");
            return;
        }

        double minutos = segundos / 60.0;
        double km = distancia / 1000.0;
        double resultado = km * (60.0 / minutos);

        if (resultado <= maxima){
            System.out.println("OK");
        } else if (resultado >= maxima20){
            System.out.println("PUNTOS");
        } else {
            System.out.println("MULTA");
        }

    }
}

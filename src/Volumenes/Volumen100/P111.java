// 111-Aprobar Quimica-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=111
package Volumenes.Volumen100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P111 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (!linea.equals("Exit")){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String linea) throws IOException {
        int n = Integer.parseInt(br.readLine());

        String[] chars = {"1s", "2s", "2p", "3s", "3p", "4s", "3d", "4p", "5s", "4d", "5p", "6s", "4f", "5d", "6p", "7s", "5f", "6d", "7p"};
        int[] arr = {2, 2, 6, 2, 6, 2, 10, 6, 2, 10, 6, 2, 14, 10, 6, 2, 14, 10, 6};

        for (int i = 0; i < arr.length; i++) {
            int o = arr[i];

            if (n <= o){
                System.out.println(chars[i] + n);
                break;
            } else {
                System.out.print(chars[i] + o + " ");
                n -= o;
            }
        }
    }
}

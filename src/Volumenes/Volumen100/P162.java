// 162-Tableros De Ajedrez-Accepted-https://www.aceptaelreto.com/problem/statistics.php?id=162
package Volumenes.Volumen100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P162 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        String[] split = linea.split(" ");
        int num = Integer.parseInt(split[0]);
        while (num != 0){
            caso(num, split[1]);
            linea = br.readLine();
            split = linea.split(" ");
            num = Integer.parseInt(split[0]);
        }
    }

    static void caso(int num, String kar){


        String blanco = "";
        String negro = "";
        String borde = "";
        for (int i = 0; i < num; i++) {
            borde += "-";
            blanco += " ";
            negro += kar;
        }

        boolean b = true;
        int escaques = 0;
        String linea = "";
        String linea2 = "";
        String linea3 = "";
        while (escaques < 8){
            if (b){
                linea3 += borde;
                linea += blanco;
                linea2 += negro;
                b = false;
            } else {
                linea3 += borde;
                linea += negro;
                linea2 += blanco;
                b = true;
            }
            escaques++;
        }

        System.out.println("|" + linea3 + "|");
        int tmp = 0;
        b = true;
        while (tmp < 8){
            if (b){
                for (int i = 0; i < num; i++) {
                    System.out.println("|" + linea + "|");
                }
                b = false;
            } else {
                for (int i = 0; i < num; i++) {
                    System.out.println("|" + linea2 + "|");
                }
                b = true;
            }
            tmp++;
        }
        System.out.println("|" + linea3 + "|");
    }
}

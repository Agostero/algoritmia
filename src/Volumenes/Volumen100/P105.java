// 105-Ventas-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=105
package Volumenes.Volumen100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P105 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (!linea.equals("-1")){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String linea) throws IOException {
        double n1 = Double.parseDouble(linea);
        double max = n1;
        double min = n1;
        double total = n1;

        String[] dias = {"MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO", "DOMINGO"};
        String diaMenos = "MARTES";
        String diaMas = "MARTES";

        double[] nums = new double[6];
        nums[0] = n1;
        double domingo = 0f;
        for (int i = 1; i < 6; i++) {
            double n = Double.parseDouble(br.readLine());
            nums[i] = n;

            if (i == 5){
                domingo = n;
            }

            total += n;

            if (n > max){
                max = n;
                diaMas = dias[i];
            }

            if (n < min){
                min = n;
                diaMenos = dias[i];
            }
        }

        double media = total / 6;
        String out = domingo > media ? "SI" : "NO";

        boolean empateMin = false;
        boolean empateMax = false;
        int totalMin = 0;
        int totalMax = 0;
        for (int i = 0; i < 6; i++) {
            if (nums[i] == max){
                totalMax++;
            }

            if (nums[i] == min){
                totalMin++;
            }

        }

        if (totalMax > 1) empateMax = true;
        if (totalMin > 1) empateMin = true;

        System.out.println((empateMax ? "EMPATE" : diaMas) + " " + (empateMin ? "EMPATE" : diaMenos) + " " + out);
    }
}

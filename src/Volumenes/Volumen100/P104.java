// 104-Moviles-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=104
package Volumenes.Volumen100;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class P104 {

    static class Reader
    {
        final private int BUFFER_SIZE = 1 << 16;
        private DataInputStream din;
        private byte[] buffer;
        private int bufferPointer, bytesRead;

        public Reader()
        {
            din = new DataInputStream(System.in);
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }

        public Reader(String file_name) throws IOException
        {
            din = new DataInputStream(new FileInputStream(file_name));
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }

        public String readLine() throws IOException
        {
            byte[] buf = new byte[4]; // line length
            int cnt = 0, c;
            while ((c = read()) != -1)
            {
                if (c == '\n')
                    break;
                buf[cnt++] = (byte) c;
            }
            return new String(buf, 0, cnt);
        }

        public int nextInt() throws IOException
        {
            int ret = 0;
            byte c = read();
            while (c <= ' ')
                c = read();
            boolean neg = (c == '-');
            if (neg)
                c = read();
            do
            {
                ret = ret * 10 + c - '0';
            }  while ((c = read()) >= '0' && c <= '9');

            if (neg)
                return -ret;
            return ret;
        }

        public long nextLong() throws IOException
        {
            long ret = 0;
            byte c = read();
            while (c <= ' ')
                c = read();
            boolean neg = (c == '-');
            if (neg)
                c = read();
            do {
                ret = ret * 10 + c - '0';
            }
            while ((c = read()) >= '0' && c <= '9');
            if (neg)
                return -ret;
            return ret;
        }

        public double nextDouble() throws IOException
        {
            double ret = 0, div = 1;
            byte c = read();
            while (c <= ' ')
                c = read();
            boolean neg = (c == '-');
            if (neg)
                c = read();

            do {
                ret = ret * 10 + c - '0';
            }
            while ((c = read()) >= '0' && c <= '9');

            if (c == '.')
            {
                while ((c = read()) >= '0' && c <= '9')
                {
                    ret += (c - '0') / (div *= 10);
                }
            }

            if (neg)
                return -ret;
            return ret;
        }

        private void fillBuffer() throws IOException
        {
            bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
            if (bytesRead == -1)
                buffer[0] = -1;
        }

        private byte read() throws IOException
        {
            if (bufferPointer == bytesRead)
                fillBuffer();
            return buffer[bufferPointer++];
        }

        public void close() throws IOException
        {
            if (din == null)
                return;
            din.close();
        }
    }

    static Reader sc;
    static Boolean balanceado = true;

    public static void main(String[] args) throws IOException {
        sc = new Reader();

        int pesoI = sc.nextInt();
        int distI = sc.nextInt();
        int pesoD = sc.nextInt();
        int distD = sc.nextInt();

        while (pesoI != 0 || distI != 0 || pesoD != 0 || distD != 0){
            caso(pesoI, distI, pesoD, distD);

            if (balanceado){
                System.out.println("SI");
            } else {
                System.out.println("NO");
            }

            balanceado = true;

            pesoI = sc.nextInt();
            distI = sc.nextInt();
            pesoD = sc.nextInt();
            distD = sc.nextInt();
        }
    }

    static class Returned {
        boolean balanceado;
        int peso;

        public Returned(boolean balanceado, int peso) {
            this.balanceado = balanceado;
            this.peso = peso;
        }
    }

    static Returned caso(int pesoI, int distI, int pesoD, int distD) throws IOException {
        if (pesoI == 0){
            Returned r = caso(sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt());
            pesoI = r.peso;
        }

        if (pesoD == 0){
            Returned r = caso(sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt());
            pesoD = r.peso;
        }


        if ((pesoI * distI) == (pesoD * distD)){
            return new Returned(true, pesoD + pesoI);
        } else {
            balanceado = false;
            return new Returned(false, pesoD + pesoI);
        }
    }
}

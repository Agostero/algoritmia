// 176-Campo De Minas-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=176
package Volumenes.Volumen100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P176 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        String[] split = linea.split(" ");
        int n1 = Integer.parseInt(split[0]);
        int n2 = Integer.parseInt(split[1]);
        while (n1 != 0 && n2 != 0){
            caso(n1, n2);
            linea = br.readLine();
            split = linea.split(" ");
            n1 = Integer.parseInt(split[0]);
            n2 = Integer.parseInt(split[1]);
        }
    }

    static void caso(int n1, int n2) throws IOException{
        char[][] tablero = new char[n2][n1];
        for (int i = 0; i < n2; i++) {
            char[] arr = br.readLine().toCharArray();
            for (int j = 0; j < n1; j++) {
                tablero[i][j] = arr[j];
            }
        }

        int total = 0;
        for (int i = 1; i < n2 - 1; i++) {
            for (int j = 1; j < n1 - 1; j++) {
                char current = tablero[i][j];
                if (current == '-'){
                    char a = tablero[i + 1][j];
                    char b = tablero[i - 1][j];
                    char c = tablero[i][j + 1];
                    char d = tablero[i][j - 1];
                    char e = tablero[i + 1][j + 1];
                    char f = tablero[i + 1][j - 1];
                    char g = tablero[i - 1][j + 1];
                    char h = tablero[i - 1][j - 1];

                    int tmp = 0;
                    if (a == '*') tmp++;
                    if (b == '*') tmp++;
                    if (c == '*') tmp++;
                    if (d == '*') tmp++;
                    if (e == '*') tmp++;
                    if (f == '*') tmp++;
                    if (g == '*') tmp++;
                    if (h == '*') tmp++;

                    if (tmp >= 6){
                        total++;
                    }
                }
            }
        }

        System.out.println(total);
    }
}

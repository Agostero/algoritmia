// 109-Liga De Padel-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=109
package Volumenes.Volumen100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class P109 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (!linea.equals("FIN")){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String linea) throws IOException {
        class Equipo {
            String nombre;
            int puntos;

            public Equipo(String nombre, int puntos) {
                this.nombre = nombre;
                this.puntos = puntos;
            }

            @Override
            public String toString() {
                return "Equipo{" +
                        "nombre='" + nombre + '\'' +
                        ", puntos=" + puntos +
                        '}';
            }
        }

        ArrayList<Equipo> equipos = new ArrayList<>();
        linea = br.readLine();
        int totalPartidos = 0;
        while (!linea.equals("FIN")){
            totalPartidos++;
            String[] split = linea.split(" ");
            String nombreLocal = split[0];
            int puntLocal = Integer.parseInt(split[1]);
            String nombreVisitante = split[2];
            int puntVisitante = Integer.parseInt(split[3]);

            int size = equipos.size();
            boolean existeLocal = false;
            boolean existeVisitante = false;
            for (int i = 0; i < size; i++) {
                Equipo e = equipos.get(i);
                if (e.nombre.equals(nombreLocal)){
                    existeLocal = true;
                    if (puntLocal > puntVisitante){
                        e.puntos += 2;
                    } else {
                        e.puntos += 1;
                    }
                }

                if (e.nombre.equals(nombreVisitante)){
                    existeVisitante = true;
                    if (puntLocal < puntVisitante){
                        e.puntos += 2;
                    } else {
                        e.puntos += 1;
                    }
                }
            }

            if (!existeLocal){
                if (puntLocal > puntVisitante){
                    equipos.add(new Equipo(nombreLocal, 2));
                } else {
                    equipos.add(new Equipo(nombreLocal, 1));
                }
            }

            if (!existeVisitante) {
                if (puntLocal > puntVisitante){
                    equipos.add(new Equipo(nombreVisitante, 1));
                } else {
                    equipos.add(new Equipo(nombreVisitante, 2));
                }
            }

            linea = br.readLine();
        }

        int partidosRestantes = (equipos.size() * (equipos.size() - 1)) - totalPartidos;

        int max = Integer.MIN_VALUE;
        int max2 = Integer.MIN_VALUE;
        String nombre = "";
        for (Equipo e : equipos){
            if (e.puntos >= max){
                max2 = max;
                max = e.puntos;
                nombre = e.nombre;
            }
        }

        if (max2 == max){
            System.out.println("EMPATE " + partidosRestantes);
        } else {
            System.out.println(nombre + " " + partidosRestantes);
        }
    }
}

// 130-Mejor... Imposible-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=130
package Volumenes.Volumen100;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class P130 {
    static class Reader
    {
        final private int BUFFER_SIZE = 1 << 16;
        private DataInputStream din;
        private byte[] buffer;
        private int bufferPointer, bytesRead;

        public Reader()
        {
            din = new DataInputStream(System.in);
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }

        public Reader(String file_name) throws IOException
        {
            din = new DataInputStream(new FileInputStream(file_name));
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }

        public String readLine() throws IOException
        {
            byte[] buf = new byte[64]; // line length
            int cnt = 0, c;
            while ((c = read()) != -1)
            {
                if (c == '\n')
                    break;
                buf[cnt++] = (byte) c;
            }
            return new String(buf, 0, cnt);
        }

        public int nextInt() throws IOException
        {
            int ret = 0;
            byte c = read();
            while (c <= ' ')
                c = read();
            boolean neg = (c == '-');
            if (neg)
                c = read();
            do
            {
                ret = ret * 10 + c - '0';
            }  while ((c = read()) >= '0' && c <= '9');

            if (neg)
                return -ret;
            return ret;
        }

        public long nextLong() throws IOException
        {
            long ret = 0;
            byte c = read();
            while (c <= ' ')
                c = read();
            boolean neg = (c == '-');
            if (neg)
                c = read();
            do {
                ret = ret * 10 + c - '0';
            }
            while ((c = read()) >= '0' && c <= '9');
            if (neg)
                return -ret;
            return ret;
        }

        public double nextDouble() throws IOException
        {
            double ret = 0, div = 1;
            byte c = read();
            while (c <= ' ')
                c = read();
            boolean neg = (c == '-');
            if (neg)
                c = read();

            do {
                ret = ret * 10 + c - '0';
            }
            while ((c = read()) >= '0' && c <= '9');

            if (c == '.')
            {
                while ((c = read()) >= '0' && c <= '9')
                {
                    ret += (c - '0') / (div *= 10);
                }
            }

            if (neg)
                return -ret;
            return ret;
        }

        private void fillBuffer() throws IOException
        {
            bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
            if (bytesRead == -1)
                buffer[0] = -1;
        }

        private byte read() throws IOException
        {
            if (bufferPointer == bytesRead)
                fillBuffer();
            return buffer[bufferPointer++];
        }

        public void close() throws IOException
        {
            if (din == null)
                return;
            din.close();
        }
    }

    static Reader sc;

    public static void main(String[] args) throws IOException {
        sc = new Reader();

        int casos = sc.nextInt();
        while (casos != 0) {
            caso(casos);
            casos = sc.nextInt();
        }
    }

    static void caso(int tam) throws IOException {
        int posicionInicialFila = sc.nextInt();
        int posicionInicialColumna = sc.nextInt();

        int posicionFinalFila = sc.nextInt();
        int posicionFinalColumna = sc.nextInt();

        if (posicionInicialFila == posicionFinalFila && posicionInicialColumna == posicionFinalColumna){
            System.out.println("0");
            return;
        }

        boolean inicialNegra = false;
        if (posicionInicialFila % 2 == 0 && posicionInicialColumna % 2 == 0){
            inicialNegra = true;
        } else if (posicionInicialFila % 2 != 0 && posicionInicialColumna % 2 != 0){
            inicialNegra = true;
        }

        boolean finalNegra = false;
        if (posicionFinalFila % 2 == 0 && posicionFinalColumna % 2 == 0){
            finalNegra = true;
        } else if (posicionFinalFila % 2 != 0 && posicionFinalColumna % 2 != 0){
            finalNegra = true;
        }

        if (finalNegra != inicialNegra){
            System.out.println("IMPOSIBLE");
            return;
        }

        if (Math.abs(posicionInicialFila - posicionFinalFila) == Math.abs(posicionInicialColumna - posicionFinalColumna)){
            System.out.println("1");
        } else {
            System.out.println("2");
        }
    }
}

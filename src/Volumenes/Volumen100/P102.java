// 102-Encriptacion De Mensajes-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=102
package Volumenes.Volumen100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P102 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (linea != null){
            String salida = caso(linea);
            if (salida.equals("FIN")){
                break;
            }
            linea = br.readLine();
        }
    }

    static String caso(String linea){
        // Minuscula: 97 - 122
        // Mayuscula: 65 - 90
        int charInicial = 112;

        int codigo = (int) linea.charAt(0);
        char[] frase = linea.substring(1).toCharArray();
        int length = frase.length;

        int codigoCesar = codigo - charInicial;

        int total = 0;
        String salida = "";
        for (int i = 0; i < length; i++){
            int c = (int) frase[i];

            if (c >= 97 && c <= 122){
                c -= codigoCesar;
                if (c > 122){
                    int restante = c - 122;
                    c = 96 + restante;

                } else if (c < 97){
                    int restante = 97 - c;
                    c = 123 - restante;
                }

                if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U'){
                    total++;
                }
            } else if (c >= 65 && c <= 90){
                c -= codigoCesar;
                if (c > 90){
                    int restante = c - 90;
                    c = 64 + restante;
                } else if (c < 65){
                    int restante = 65 - c;
                    c = 91 - restante;
                }

                if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U'){
                    total++;
                }
            }
            salida += (char) c;
        }

        // System.out.println(salida);

        if (salida.equals("FIN")){
            return salida;
        } else {
            System.out.println(total);
            return "";
        }
    }
}

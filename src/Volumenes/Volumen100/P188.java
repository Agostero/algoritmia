// 188-Encadenando Palabras-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=188
package Volumenes.Volumen100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P188 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (linea != null){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String linea){
        String[] palabras = linea.split(" ");
        int length = palabras.length;

        for (int i = 0; i < length - 1; i++) {
            String current = palabras[i];
            String next = palabras[i + 1];

            String currentSilaba = current.substring(current.length() - 2);
            String nextSilaba = next.substring(0, 2);

            if (!currentSilaba.equalsIgnoreCase(nextSilaba)){
                System.out.println("NO");
                return;
            }
        }

        System.out.println("SI");
    }
}

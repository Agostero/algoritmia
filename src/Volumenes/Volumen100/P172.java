// 172-El Pan En Las Bodas-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=172
package Volumenes.Volumen100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P172 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        String[] split = linea.split(" ");
        int sillas = Integer.parseInt(split[0]);
        while (sillas != 0){
            caso(split, sillas);
            linea = br.readLine();
            split = linea.split(" ");
            sillas = Integer.parseInt(split[0]);
        }
    }

    static void caso(String[] split, int sillas) throws IOException {
        split = split[1].split("(?!^)");
        int numIzquierda = 0;
        int numDerecha = 0;

        for (int i = 0; i < sillas; i++) {
            String c = split[i];
            if (c.equals("I")){
                numIzquierda++;
                if (numDerecha > 0) break;
            } else if (c.equals("D")){
                numDerecha++;
                if (numIzquierda > 0) break;
            }
        }

        if (numIzquierda > 0 && numDerecha > 0){
            System.out.println("ALGUNO NO COME");
        } else {
            System.out.println("TODOS COMEN");
        }
    }
}

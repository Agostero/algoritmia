// 194-Salvemos Al Lince Iberico-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=194
package Volumenes.Volumen100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P194 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        int casos = Integer.parseInt(br.readLine());
        for (int i = 0; i < casos; i++) {
            caso();
        }
    }

    static void caso() throws IOException {
        String linea = br.readLine();
        String[] split = linea.split("(?!^)");
        int l = split.length;
        int total = 0;
        for (int i = 0; i < l; ) {
            String actual = split[i];
            if (actual.equals("X")){
                i++;
            } else {
                total++;
                i += 3;
            }
        }

        System.out.println(total);
    }
}

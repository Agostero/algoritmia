// 106-Codigos De Barras-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=106
package Volumenes.Volumen100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P106 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while(!linea.equals("0")){
            caso(linea);
            linea = br.readLine();
        }
    }

    static void caso(String linea){
        boolean ean8 = true;

        int length = linea.length();

        int dif = 0;
        if (length <= 8){
            dif = 8 - length;
        } else if (length <= 13){
            ean8 = false;
            dif = 13 - length;
        }

        for (int i = 0; i < dif; i++) {
            linea = "0" + linea;
        }

        String[] split = linea.split("(?!^)");
        int ultimo = Integer.parseInt(split[split.length - 1]);
        int total = 0;
        boolean porTres = true;
        for (int i = split.length - 2; i >= 0; i--) {
            int n = Integer.parseInt(split[i]);

            if (porTres){
                total += (n * 3);
                porTres = false;
            } else {
                total += n;
                porTres = true;
            }
        }

        int restante;
        if (total % 10 == 0){
            restante = 0;
        } else {
            restante = 10 - (total % 10);
        }

        if (restante == ultimo){
            if (!ean8){
                String pais = "Desconocido";

                String d1 = linea.substring(0, 1);
                String d2 = linea.substring(0, 2);
                String d3 = linea.substring(0, 3);

                if (d1.equals("0")){
                    pais = "EEUU";
                } else if (d2.equals("50")){
                    pais = "Inglaterra";
                } else if (d2.equals("70")){
                    pais = "Noruega";
                } else if (d3.equals("380")){
                    pais = "Bulgaria";
                } else if (d3.equals("539")){
                    pais = "Irlanda";
                } else if (d3.equals("560")){
                    pais = "Portugal";
                } else if (d3.equals("759")){
                    pais = "Venezuela";
                } else if (d3.equals("850")){
                    pais = "Cuba";
                } else if (d3.equals("890")){
                    pais = "India";
                }

                System.out.println("SI " + pais);

            } else {
                System.out.println("SI");
            }
        } else {
            System.out.println("NO");
        }
    }
}

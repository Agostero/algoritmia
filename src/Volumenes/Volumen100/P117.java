// 117-La Fiesta Aburrida-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=117
package Volumenes.Volumen100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P117 {

    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        int casos = Integer.parseInt(br.readLine());
        for (int i = 0; i < casos; i++) {
            String linea = br.readLine();
            System.out.println("Hola, " + linea.substring(4) + ".");
        }
    }
}

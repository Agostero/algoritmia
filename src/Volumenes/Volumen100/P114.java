// 114-Ultimo Digito Del Factorial-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=114
package Volumenes.Volumen100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P114 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        int casos = Integer.parseInt(br.readLine());
        for (int i = 0; i < casos; i++) {
            caso();
        }
    }

    static void caso() throws IOException {
        long n = Long.parseLong(br.readLine());

        if (n == 0){
            System.out.println("1");
        } else if (n == 1){
            System.out.println("1");
        } else if (n == 2){
            System.out.println("2");
        } else if (n == 3){
            System.out.println("6");
        } else if (n == 4){
            System.out.println("4");
        } else {
            System.out.println("0");
        }
    }
}

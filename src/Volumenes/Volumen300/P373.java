// 373-Cubos Visibles-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=373&cat=102
package Volumenes.Volumen300;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class P373 {

    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        int casos = Integer.parseInt(br.readLine());

        for (int i = 0; i < casos; i++) {
            caso();
        }
    }

    public static void caso() throws IOException {
        int dimensiones = Integer.parseInt(br.readLine());

        BigInteger dim = new BigInteger(String.valueOf(dimensiones));
        BigInteger dim2 = new BigInteger(String.valueOf(dimensiones - 2));
        BigInteger total = new BigInteger("0");
        BigInteger total2 = new BigInteger("0");
        BigInteger finalValue;

        total = total.add(dim.multiply(dim).multiply(dim));
        total2 = total2.add(dim2.multiply(dim2).multiply(dim2));
        finalValue = total.subtract(total2);

        System.out.println(finalValue);
    }
}

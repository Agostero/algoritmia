// 366-Los Niños Buenos-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=366&cat=98
package Volumenes.Volumen300;

import java.util.Arrays;
import java.util.Scanner;

public class P366 {
    public static void main(String[] args) {
        class Nino implements Comparable<Nino> {
            int bueno;
            int peso;

            @Override
            public int compareTo(Nino o) {
                if (bueno != o.bueno){
                    return bueno > o.bueno ? -1 : 1;
                }

                return peso > o.peso ? 1 : -1;

                /*if (bueno > o.bueno){
                    return -1;
                } else if (bueno == o.bueno){
                    if (peso > o.peso){
                        return 1;
                    } else {
                        return -1;
                    }
                } else {
                    return 1;
                }*/
            }
        }

        Scanner sc = new Scanner(System.in);

        int num;

        while (true){
            num = sc.nextInt();

            if (num == 0){
                break;
            }

            Nino[] ninos = new Nino[num];
            for (int i = 0; i < num; i++) {
                Nino n = new Nino();
                n.bueno = sc.nextInt();
                n.peso = sc.nextInt();
                ninos[i] = n;
            }

            Arrays.sort(ninos);

            for (int i = 0; i < ninos.length; i++) {
                System.out.println(ninos[i].bueno + " " + ninos[i].peso);
            }
            System.out.println();
        }
    }
}

// 364-Espionaje En Navidad-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=364&cat=98
package Volumenes.Volumen300;

import java.util.Scanner;

public class P364 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (true){
            String linea = sc.nextLine();

            if (linea.equalsIgnoreCase("FIN")){
                break;
            }

            char[] caracteres = linea.toCharArray();

            for (int i = 0; i < caracteres.length; i++) {
                if (caracteres[i] == 'Z'){
                    caracteres[i] = 'A';
                } else if (caracteres[i] != ' '){
                    caracteres[i] = (char) (caracteres[i] + 1);
                }
            }

            for (int i = 0; i < caracteres.length; i++) {
                System.out.print(caracteres[i]);
            }
            System.out.println();
        }
    }
}

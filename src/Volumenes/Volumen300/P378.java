// 378-La Justicia De La Loteria-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=378&cat=102
package Volumenes.Volumen300;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

public class P378 {

    static class Reader
    {
        final private int BUFFER_SIZE = 1 << 16;
        private DataInputStream din;
        private byte[] buffer;
        private int bufferPointer, bytesRead;

        public Reader()
        {
            din = new DataInputStream(System.in);
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }

        public Reader(String file_name) throws IOException
        {
            din = new DataInputStream(new FileInputStream(file_name));
            buffer = new byte[BUFFER_SIZE];
            bufferPointer = bytesRead = 0;
        }

        public String readLine() throws IOException
        {
            byte[] buf = new byte[64]; // line length
            int cnt = 0, c;
            while ((c = read()) != -1)
            {
                if (c == '\n')
                    break;
                buf[cnt++] = (byte) c;
            }
            return new String(buf, 0, cnt);
        }

        public int nextInt() throws IOException
        {
            int ret = 0;
            byte c = read();
            while (c <= ' ')
                c = read();
            boolean neg = (c == '-');
            if (neg)
                c = read();
            do
            {
                ret = ret * 10 + c - '0';
            }  while ((c = read()) >= '0' && c <= '9');

            if (neg)
                return -ret;
            return ret;
        }

        public long nextLong() throws IOException
        {
            long ret = 0;
            byte c = read();
            while (c <= ' ')
                c = read();
            boolean neg = (c == '-');
            if (neg)
                c = read();
            do {
                ret = ret * 10 + c - '0';
            }
            while ((c = read()) >= '0' && c <= '9');
            if (neg)
                return -ret;
            return ret;
        }

        public double nextDouble() throws IOException
        {
            double ret = 0, div = 1;
            byte c = read();
            while (c <= ' ')
                c = read();
            boolean neg = (c == '-');
            if (neg)
                c = read();

            do {
                ret = ret * 10 + c - '0';
            }
            while ((c = read()) >= '0' && c <= '9');

            if (c == '.')
            {
                while ((c = read()) >= '0' && c <= '9')
                {
                    ret += (c - '0') / (div *= 10);
                }
            }

            if (neg)
                return -ret;
            return ret;
        }

        private void fillBuffer() throws IOException
        {
            bytesRead = din.read(buffer, bufferPointer = 0, BUFFER_SIZE);
            if (bytesRead == -1)
                buffer[0] = -1;
        }

        private byte read() throws IOException
        {
            if (bufferPointer == bytesRead)
                fillBuffer();
            return buffer[bufferPointer++];
        }

        public void close() throws IOException
        {
            if (din == null)
                return;
            din.close();
        }
    }

    static Reader sc;

    public static void main(String[] args) throws IOException {
        sc = new Reader();

        int localidades = sc.nextInt();
        while (localidades != 0){
            caso(localidades);
            localidades = sc.nextInt();
        }
    }

    public static void caso(int localidades) throws IOException {

        class Pack implements Comparable<Pack>{
            int inversion;
            int premios;

            public Pack(int inversion, int premios){
                this.inversion = inversion;
                this.premios = premios;
            }

            @Override
            public int compareTo(Pack o) {
                if (inversion != o.inversion){
                    return inversion > o.inversion ? 1 : -1;
                }

                return premios > o.premios ? 1 : -1;
            }

            @Override
            public String toString() {
                return "Pack{" +
                        "inversion=" + inversion +
                        ", premios=" + premios +
                        '}';
            }
        }

        Pack[] packs = new Pack[localidades];

        for (int i = 0; i < localidades; i++) {
            int inversion = sc.nextInt();
            int premios = sc.nextInt();

            packs[i] = new Pack(inversion, premios);
        }

        Arrays.sort(packs);

        Pack previous = packs[0];
        String out = "SI";
        for (int i = 1; i < localidades; i++) {
            Pack p = packs[i];

            if (p.inversion > previous.inversion){
                if (p.premios <= previous.premios){
                    out = "NO";
                    break;
                }
            }

            previous = p;
        }

        System.out.println(out);
    }
}

// 376-Siete Picos-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=376&cat=102
package Volumenes.Volumen300;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P376 {

    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        while(true){
            int veces = Integer.parseInt(br.readLine());

            if (veces == 0){
                break;
            }

            caso();
        }
    }

    public static void caso() throws IOException {
        String linea = br.readLine();
        String[] split = linea.split(" ");

        int length = split.length;
        int count = 0;
        for (int i = 0; i < length; i++) {
            int nAnterior;
            int nPosterior;
            int actual = Integer.parseInt(split[i]);

            if (i == 0){
                nAnterior = Integer.parseInt(split[length - 1]);
                nPosterior = Integer.parseInt(split[i + 1]);
            } else if (i == length - 1){
                nAnterior = Integer.parseInt(split[i - 1]);
                nPosterior = Integer.parseInt(split[0]);
            } else {
                nAnterior = Integer.parseInt(split[i - 1]);
                nPosterior = Integer.parseInt(split[i + 1]);
            }

            if (nAnterior < actual && actual > nPosterior){
                count++;
            }
        }

        System.out.println(count);
    }
}

// 375-Pistas De Aterrizaje-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=375&cat=102
package Volumenes.Volumen300;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P375 {

    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = "";
        while((linea = br.readLine()) != null){
            String[] split = linea.split("(?!^)");
            int num = Integer.parseInt(split[0] + split[1]);

            String dir = "";
            if (split.length > 2){
                dir = split[2];
            }

            if (dir.equals("R")){
                dir = "L";
            } else if (dir.equals("L")){
                dir = "R";
            }

            if (num + 18 > 36){
                int rest = (num + 18) - 36;
                num = rest;
            } else {
                num += 18;
            }

            String out = "";
            if (num < 10){
                out += "0" + num;
            } else {
                out += num;
            }

            if (dir != ""){
                out += dir;
            }

            System.out.println(out);
        }
    }
}

// 362-El Dia De Navidad-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=362&cat=98
package Volumenes.Volumen300;

import java.util.Scanner;

public class P362 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int dia, mes;
        for (int i = 0; i < n; i++) {
            dia = sc.nextInt();
            mes = sc.nextInt();

            if (dia == 25 && mes == 12){
                System.out.println("SI");
            } else {
                System.out.println("NO");
            }
        }
    }
}

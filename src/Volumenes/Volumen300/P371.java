// 371-Aburrimiento En Las Sobremesas-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=371&cat=102
package Volumenes.Volumen300;

import java.util.Scanner;

public class P371 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while(true){
            int num = sc.nextInt();
            if (num == 0){
                break;
            }

            int lados = 1, interiores = 0, total = 3;
            while (lados < num){
                lados++;
                interiores = total;
                total = (lados * 3) + interiores;
            }

            System.out.println(total);
        }
    }
}

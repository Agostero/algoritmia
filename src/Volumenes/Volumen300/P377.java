// 377-Cuadrados Imperfectos-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=377&cat=102
package Volumenes.Volumen300;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class P377 {

    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        while (true){
            String linea = br.readLine();
            if (linea == null){
                break;
            }

            BigInteger b = new BigInteger(linea);


            BigInteger remainder = b.remainder(new BigInteger("16"));

            String lastLetter = String.valueOf(remainder);

            if (remainder.equals(new BigInteger("10"))){
                lastLetter = "a";
            } else if (remainder.equals(new BigInteger("11"))){
                lastLetter = "b";
            } else if (remainder.equals(new BigInteger("12"))){
                lastLetter = "c";
            } else if (remainder.equals(new BigInteger("13"))){
                lastLetter = "d";
            } else if (remainder.equals(new BigInteger("14"))){
                lastLetter = "e";
            } else if (remainder.equals(new BigInteger("15"))){
                lastLetter = "f";
            }

            if (lastLetter.equals("0") || lastLetter.equals("1") || lastLetter.equals("4") || lastLetter.equals("9")){
                System.out.println("NO SE");
            } else {
                System.out.println("IMPERFECTO");
            }
        }
    }
}

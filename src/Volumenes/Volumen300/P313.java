// 313-Fin De Mes-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=313
package Volumenes.Volumen300;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P313 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int casos = Integer.parseInt(br.readLine());

        for (int i = 0; i < casos; i++) {
            String linea = br.readLine();
            String[] lineaArray = linea.split(" ");

            int saldo = Integer.parseInt(lineaArray[0]);
            int cambio = Integer.parseInt(lineaArray[1]);

            if (saldo + cambio >= 0){
                System.out.println("SI");
            } else {
                System.out.println("NO");
            }
        }
    }
}

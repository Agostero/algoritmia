// 370-La 13 14-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=370&cat=102
package Volumenes.Volumen300;

import java.util.Scanner;

public class P370 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int casos = sc.nextInt();
        sc.nextLine();

        for (int i = 0; i < casos; i++) {
            String[] linea = sc.nextLine().split("-");
            int n1 = Integer.parseInt(linea[0]);
            int n2 = Integer.parseInt(linea[1]);

            if (n1 == n2){
                System.out.println("NO");
                continue;
            }

            int min = n1 < n2 ? n1 : n2;
            int max = n1 > n2 ? n1 : n2;

            if (min % 2 == 0){
                if (max - min == 1){
                    System.out.println("SI");
                } else {
                    System.out.println("NO");
                }
            } else {
                System.out.println("NO");
            }
        }
    }
}

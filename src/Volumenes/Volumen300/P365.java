// 365-En La Cola De Papa Noel-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=365&cat=98
package Volumenes.Volumen300;

import java.util.Scanner;

public class P365 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int casos = sc.nextInt();

        int enCola, posicion, tiempo = 0;
        for (int i = 0; i < casos; i++) {

            enCola = sc.nextInt();
            posicion = sc.nextInt() - 1;

            int[] regalos = new int[enCola];
            for (int j = 0; j < enCola; j++) {
                regalos[j] = sc.nextInt();
            }

            // 2 2 2

            while (regalos[posicion] > 0){
                for (int j = 0; j < enCola; j++) {
                    if (regalos[j] > 0){
                        tiempo += 2;
                    }

                    regalos[j]--;

                    if (j == posicion && regalos[j] == 0){
                        break;
                    }
                }
            }

            System.out.println(tiempo);
            tiempo = 0;
        }
    }
}

// 331-Parrilla De Salida-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=331&cat=93
package Volumenes.Volumen300;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class P331 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        String linea = br.readLine();
        while (!linea.equals("0")){
            caso(Integer.parseInt(linea));
            linea = br.readLine();
        }
    }

    static void caso(int num) throws IOException {

        class Piloto implements Comparable<Piloto> {
            int llegada;
            int avances;
            String nombre;
            int real;
            boolean posible = true;

            public Piloto(int llegada, int avances, String nombre, int real) {
                this.llegada = llegada;
                this.avances = avances;
                this.nombre = nombre;
                this.real = real;
            }

            @Override
            public int compareTo(Piloto o) {
                if (real == o.real){
                    posible = false;
                    return 0;
                } else if (real > o.real){
                    return 1;
                } else {
                    return -1;
                }
            }

            @Override
            public String toString() {
                return "Piloto{" +
                        "llegada=" + llegada +
                        ", avances=" + avances +
                        ", nombre='" + nombre + '\'' +
                        ", real=" + real +
                        '}';
            }
        }

        boolean possible = true;

        Piloto[] pilotos = new Piloto[num];
        for (int i = 0; i < num; i++) {
            String linea = br.readLine();
            String[] split = linea.split(" ", 2);
            int avances = Integer.parseInt(split[0]);
            String nombre = split[1];
            int real = (i + 1) + avances;

            if (real > 26 || real < 1){
                possible = false;
            }

            pilotos[i] = new Piloto(i + 1, avances, nombre, real);
        }

        Arrays.sort(pilotos);
        for (Piloto p : pilotos){
            if (!p.posible){
                possible = false;
            }
        }


        if (possible){
            for (Piloto p : pilotos){
                System.out.println(p.real + " " + p.nombre);
            }
        } else {
            System.out.println("IMPOSIBLE");
        }

        System.out.println("-----");
    }
}

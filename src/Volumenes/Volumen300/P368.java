// 368-Cociendo Huevos-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=368&cat=102
package Volumenes.Volumen300;

import java.util.Scanner;

public class P368 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (true){
            String linea = sc.nextLine();
            String[] lineaArray = linea.split(" ");

            int numHuevos = Integer.parseInt(lineaArray[0]);
            int capacidad = Integer.parseInt(lineaArray[1]);

            if (numHuevos == 0 && capacidad == 0){
                break;
            }

            int tiempo = 0;
            if (numHuevos > capacidad) {
                while(true){
                    if (numHuevos <= 0){
                        break;
                    }

                    numHuevos -= capacidad;
                    tiempo += 10;
                }
            } else {
                tiempo = 10;
            }

            System.out.println(tiempo);
        }
    }
}

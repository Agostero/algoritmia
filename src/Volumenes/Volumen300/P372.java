// 372-La Farsante De Mary Poppins-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=372&cat=102
package Volumenes.Volumen300;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class P372 {
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        br = new BufferedReader(new InputStreamReader(System.in));

        int casos = Integer.parseInt(br.readLine());

        for (int i = 0; i < casos; i++) {
            caso();
        }
    }

    public static void caso() throws IOException{
        String linea = br.readLine();
        boolean upperCase = Character.isUpperCase(linea.charAt(0));
        linea = linea.toLowerCase();
        String[] split = linea.split("(?!^)");

        String lineaInverse = "";
        int length = split.length - 1;
        for (int i = length; i >= 0; i--) {
            if (i == length && upperCase){
                lineaInverse += split[i].toUpperCase();
            } else {
                lineaInverse += split[i];
            }
        }

        System.out.println(lineaInverse);
    }
}

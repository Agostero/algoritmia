// 369-Contando En La Arena-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=369&cat=102
package Volumenes.Volumen300;

import java.util.Scanner;

public class P369 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while(true){
            int num = sc.nextInt();
            if (num == 0){
                break;
            }

            String out = "";
            for (int i = 0; i < num; i++) {
                out += "1";
            }

            System.out.println(out);
        }
    }
}

// 374-Criogenizacion-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=374&cat=102
package Volumenes.Volumen300;

import java.util.Scanner;

public class P374 {
    public static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);

        int casos = sc.nextInt();
        for (int i = 0; i < casos; i++) {
            caso();
        }
    }

    public static void caso() {
        long num = sc.nextLong();

        long minValue = num;
        long maxValue = num;

        int minCount = 1;
        int maxCount = 1;

        while (true){
            num = sc.nextLong();
            if (num == 0){
                break;
            }

            if (num < minValue){
                minValue = num;
                minCount = 1;
            } else if (num == minValue){
                minCount++;
            }

            if (num > maxValue){
                maxValue = num;
                maxCount = 1;
            } else if (num == maxValue){
                maxCount++;
            }
        }

        System.out.println(minValue +  " " +  minCount + " " + maxValue + " " + maxCount);
    }
}

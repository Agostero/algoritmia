// 363-Las Calorias-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=363&cat=98
package Volumenes.Volumen300;

import java.util.Scanner;

public class P363 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int caloriasConsumidas = -1;

        int numeroComidas, caloriasTotales = 0, numeroEntrenamientos;

        while (caloriasConsumidas != 0){
            caloriasConsumidas = sc.nextInt();

            if (caloriasConsumidas == 0){
                break;
            }

            numeroComidas = sc.nextInt();

            for (int i = 0; i < numeroComidas; i++) {
                caloriasTotales += sc.nextInt();
            }

            numeroEntrenamientos = caloriasTotales / caloriasConsumidas;

            if (caloriasTotales % caloriasConsumidas > 0){
                numeroEntrenamientos++;
            }

            System.out.println(numeroEntrenamientos);

            caloriasTotales = 0;
        }

    }
}

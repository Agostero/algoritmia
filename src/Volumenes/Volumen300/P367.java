// 367-Repartiendo Regalos En Tu Calle-Accepted-https://www.aceptaelreto.com/problem/statement.php?id=367&cat=98
package Volumenes.Volumen300;

import java.util.Scanner;

public class P367 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int casos = sc.nextInt();

        for (int i = 0; i < casos; i++) {
            int posicion = sc.nextInt();
            int numeroDePortales = sc.nextInt();

            int[] portales = new int[numeroDePortales];
            for (int j = 0; j < numeroDePortales; j++) {
                portales[j] = sc.nextInt();
            }

            int[] salida = new int[numeroDePortales];
            int portalMasCercano = 0, min = Integer.MAX_VALUE;
            for (int j = 0; j < numeroDePortales; j++) {
                for (int k = 0; k < numeroDePortales; k++) {
                    if (portales[k] != -1){
                        int distancia = Math.abs(portales[k] - posicion);
                        if (distancia == min){
                            if (portales[k] > portales[portalMasCercano]){
                                portalMasCercano = k;
                            }
                        } else if (distancia < min){
                            min = distancia;
                            portalMasCercano = k;
                        }
                    }
                }

                salida[j] = portales[portalMasCercano];
                posicion = portales[portalMasCercano];
                portales[portalMasCercano] = -1;
                min = Integer.MAX_VALUE;

            }

            for (int j = 0; j < numeroDePortales; j++) {
                if (j == numeroDePortales - 1){
                    System.out.print(salida[j] + "\n");
                } else {
                    System.out.print(salida[j] + " ");
                }
            }
        }
    }
}
